addSbtPlugin("io.spray"              % "sbt-revolver"        % "0.9.1")
addSbtPlugin("io.github.davidmweber" % "flyway-sbt"          % "7.4.0")
addSbtPlugin("com.typesafe.sbt"      % "sbt-native-packager" % "1.8.1")
addSbtPlugin("ch.epfl.scala"         % "sbt-scalafix"        % "0.9.29")
