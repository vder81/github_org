package taskforce.common

trait ResourceId[A] {
  val value: A
}
